import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import TabNavigator from './app/screens/tabNavigator';

export default class App extends Component {
  render() {
    return (
      <TabNavigator />
    );
  }
}
