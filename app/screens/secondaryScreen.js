import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View, Image } from 'react-native';
import { HoursForecast, WeekForecast, WeeksItem, HeaderComponent, AddInfoComponent } from '../components';

export default class SecondaryScreen extends Component {
    constructor(props) {
        super(props);
        this.todaysWeather = [
            ['1', 'snowy', '15'], ['2', 'moony', '16'], ['3', 'moony', '17'], ['4', 'rainy', '18'],
            ['5', 'stormy', '19'], ['6', 'rainy', '19'], ['7', 'cloudy', '20'], ['8', 'sunny', '20'],
            ['9', 'sunny', '21'], ['10', 'sunny', '21'], ['11', 'sunny', '21'], ['12', 'cloudy', '22'],
            ['13', 'cloudy', '24'], ['14', 'rainy', '25'], ['15', 'rainy', '23'], ['16', 'cloudy', '23'],
            ['17', 'cloudy', '21'], ['18', 'moony', '20'], ['19', 'moony', '19'], ['20', 'moony', '17'],
        ];
        this.weeklyWeather = [
            ['07/24', 'snowy', '21,15'], ['07/25', 'moony', '22,15'], ['07/26', 'moony', '24,17'],
            ['07/27', 'rainy', '19,15'], ['07,28', 'stormy', '19,14'], ['07/29', 'rainy', '20,15'],
            ['07/30', 'cloudy', '18,13']
        ];
        this.otherParams = ['7', '50'];
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <HeaderComponent city='Dnipro' weather='Sunny' temperature='23' textColor={'#cedaf1'}/>
                    <WeeksItem daysData={['07/24', '', '23,29']} borderEnabled={false}/>
                    <HoursForecast data={this.todaysWeather} />
                    <WeekForecast data={this.weeklyWeather} />
                    <AddInfoComponent wind={this.otherParams[0]} humidity={this.otherParams[1]} />
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#264067',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
});
