import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import MainScreen from './mainScreen';
import SecondaryScreen from './secondaryScreen';

const TabNavigator = createMaterialTopTabNavigator({
    Main: MainScreen,
    Secondary: SecondaryScreen,
}, {
        swipeEnabled: true,
        animationEnabled: true,
        showIcon: true,
        tabBarPosition: 'bottom',
        tabBarComponent: () => null,
    });

export default createAppContainer(TabNavigator);