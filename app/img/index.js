export const weatherImages = {
    sunny: require('../img/iconfinder_weather01_4102328.png'),
    cloudy: require('../img/iconfinder_weather04_4102315.png'),
    rainy: require('../img/iconfinder_weather06_4102317.png'),
    stormy: require('../img/iconfinder_weather08_4102318.png'),
    moony: require('../img/iconfinder_weather10_4102321.png'),
    snowy: require('../img/iconfinder_weather13_4102323.png')
};