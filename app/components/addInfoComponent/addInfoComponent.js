import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class AddInfoComponent extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const { wind, humidity } = this.props;
        return (
            <View style={styles.forbar}>
                <Text style={styles.text} >Wind speed: {wind} km/h</Text>
                <Text style={styles.text} >Humidity: {humidity}%</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000'
    },
    forbar: {
        width: '100%',
        //flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        //borderColor: '#000',
        //borderWidth: 1
    }
});
