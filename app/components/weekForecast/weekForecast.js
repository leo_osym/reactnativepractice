import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import WeeksItem from '../weeksItem/weeksItem';

export default class WeekForecast extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data } = this.props;
        return (
            <View style={styles.weekForecast}
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}>
                    {
                        data.map((item, index) => {
                            return (
                                <WeeksItem key={index} daysData={item} borderEnabled={true}/>
                            )
                        })
                    }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    weekForecast: {
        //height: 150,
        width: '100%',
        alignContent: 'center',
        justifyContent: 'space-between',
        //borderColor: 'gray',
        //borderWidth: 2,
        //borderRightColor: 'gray',
        //borderRightWidth: 2,
    },
});
