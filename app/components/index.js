import HeaderComponentOriginal from "./headerComponent/headerComponent";
import HoursForecastOriginal from "./hoursForecast/hoursForecast";
import HoursItemOriginal from "./hoursItem/hoursItem";
import WeekForecastOriginal from "./weekForecast/weekForecast";
import WeeksItemOriginal from "./weeksItem/weeksItem";
import AddInfoComponentOriginal from "./addInfoComponent/addInfoComponent";

export const HeaderComponent = HeaderComponentOriginal;
export const HoursForecast = HoursForecastOriginal;
export const HoursItem = HoursItemOriginal;
export const WeekForecast = WeekForecastOriginal;
export const WeeksItem = WeeksItemOriginal;
export const AddInfoComponent = AddInfoComponentOriginal;