import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class HeaderComponent extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const { city, weather, temperature, textColor } = this.props;
        return (
            <View style={styles.forbar}>
                <Text style={{fontSize: 25, fontWeight: 'bold', margin: 5, color:textColor}}>{city}</Text>
                <Text style={{fontSize: 30, fontWeight: 'bold', color:textColor}}>{weather}</Text>
                <Text style={{fontSize: 45, fontWeight: 'bold', margin: 5, color:textColor}}>{temperature}°</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    forbar: {
        //flexDirection: 'row',
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        //paddingLeft: 15,
        //paddingRight: 15,
        //borderColor: '#000',
        //borderWidth: 1
    }
});
