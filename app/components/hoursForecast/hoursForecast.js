import React, { Component } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import HoursItem from '../hoursItem/hoursItem';

export default class HoursForecast extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data } = this.props;
        return (
            <View style={styles.scroll}>
                <ScrollView
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}>
                    {
                        data.map((item, index) => {
                            return (
                                <HoursItem key={index} hoursData={item} />
                            )
                        })
                    }
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scroll: {
        height: 150,
        alignContent: 'center',
        //justifyContent: 'center',
        //borderColor: 'gray',
        //borderWidth: 2,
        //borderRightColor: 'gray',
        //borderRightWidth: 2,
    },
});
