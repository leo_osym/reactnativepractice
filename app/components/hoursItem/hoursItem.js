import React, { PureComponent } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { weatherImages } from '../../img';

export default class HoursItem extends PureComponent {
    constructor(props) {
        super(props);
        this.weather = weatherImages;
    }

    render() {
        const { hoursData, index } = this.props;
        return (
            <View key={index} style={styles.forbar}>
                <Text style={styles.text} >{hoursData[0]}</Text>
                <Image style={styles.image} source={this.weather[hoursData[1]]} />
                <Text style={styles.text} >{hoursData[2] + '°'}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000'
    },
    forbar: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        borderColor: '#000',
        borderWidth: 1
    }
});
