import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { weatherImages } from '../../img';

export default class WeeksItem extends Component {
    constructor(props) {
        super(props);
        this.weather = weatherImages;
    }

    render() {
        const { daysData, index, borderEnabled } = this.props;
        return (
            <View key={index} style={{...styles.forbar, borderBottomWidth: borderEnabled*2}}>
                <Text style={styles.text} >{daysData[0]}</Text>
                <Image style={styles.image} source={this.weather[daysData[1]]} />
                <Text style={styles.text} >{daysData[2].split(',')[0] + '°    '+daysData[2].split(',')[1]+'°'}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000'
    },
    forbar: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        borderColor: '#000',
        borderBottomWidth: 2,
        //borderWidth: 1
    }
});
