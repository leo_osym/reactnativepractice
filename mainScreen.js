import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View, Image } from 'react-native';
import { gray } from 'ansi-colors';

export default class MainScreen extends Component {
    constructor(props) {
        super(props);
        this.emojis = '😂,😘,❤️,😍,😊,😁,😔,😄,😭,💋,😒,😳,😜,🙈,😉,😃,😢,😝'.split(',');
        this.weather = {
            sunny: require('./img/iconfinder_weather01_4102328.png'),
            cloudy: require('./img/iconfinder_weather04_4102315.png'),
            rainy: require('./img/iconfinder_weather06_4102317.png'),
            stormy: require('./img/iconfinder_weather08_4102318.png'),
            moony: require('./img/iconfinder_weather10_4102321.png'),
            snowy: require('./img/iconfinder_weather13_4102323.png')
        };
        this.weatherMap = ['snowy', 'moony', 'cloudy', 'rainy',
                           'stormy', 'sunny', 'snowy', 'moony', 
                           'cloudy', 'rainy', 'stormy', 'sunny'];
        this.temperature = ['15','16','18','20','22','22','24','25','23','22','21','19'];
        this.hours = ['1','2','3','4','5','6','7','8','9','10','11','12'];
    }

    render() {
        console.log(this.emojis);
        console.log(this.weather['cloudy']);
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={{width: '100%', height: '100%'}}>
                <Text style={styles.welcome}>Welcome to React Native!</Text>
                <View style={styles.scroll}>
                    <ScrollView
                        horizontal
                        pagingEnabled
                        showsHorizontalScrollIndicator={false}
                    >
                        {
                            this.weatherMap.map((item, index) => {
                                let src = this.weather[item];
                                console.log('src', src);
                                return (
                                    <View style={styles.forbar}>
                                    <Text style={styles.text} >{this.hours[index]}</Text>
                                    <Image style={styles.image} key={index} source={src} />
                                    <Text style={styles.text} >{this.temperature[index]+'°'}</Text>
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    emoji: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    scroll: {
        height: 100,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 2,
        borderRightColor: 'gray',
        borderRightWidth: 2,
    },
    image: {
        flex: 1,
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000'
    },
    forbar: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        borderColor: '#000',
        borderWidth: 1
    }
});
